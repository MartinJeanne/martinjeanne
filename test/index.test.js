const { sum } = require("../public/index");

test('Testing sum', function () {
    expect(sum(3,5)).toBe(8);
});

test('Testing sum', function () {
    expect(sum(-8,-6)).toBe(-14);
});

test('Testing sum', function () {
    expect(sum("aa","bb")).toBe(NaN);
    expect(sum( [1,2],[1,2])).toBe(NaN);
});

test('Testing sum', function () {
    expect(sum(Number.MAX_VALUE,1000)).toBe(Number.MAX_VALUE);
});

test('Testing sum', function () {
    expect(sum(Number.MIN_VALUE, Number.MIN_VALUE)).toBe(Number.MIN_VALUE*2);
});
