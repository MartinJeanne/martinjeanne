const sum = (a, b) => {
    return a + b;
}

const sumBis = (a, b) => {
    return a + b;
}

module.exports.sum = sum;
module.exports.sumBis = sumBis;
